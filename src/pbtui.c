#include <stdlib.h>
#include <curses.h>
#include <menu.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <pthread.h>
#include <math.h>
#include <stdio.h>

#include "libdebug.h"
#include "libtty.h"
#include "window_manager.h"

#define CTRLD (4)
#define ENTER (10)
#define PBD_LEN_HS (14)
#define PBD_LEN_PACK (4)

static const long double pbd_pack_volt_calib[PBD_LEN_PACK][2] = {
    {3.3L*2.0L/4096.0L, 0.0},
    {3.3L*2.0L/4096.0L, 0.0},
    {3.3L*2.0L/4096.0L, 0.0}, 
    {3.3L*2.0L/4096.0L, 0.0}
};


static const long double pbd_hs_curr_calib[PBD_LEN_HS][2] = { 
    {0.3987341772L, 0.0},
    {0.6L, 0.0},
    {0.3987341772L, 0.0},
    {0.3987341772L, 0.0},
    {0.3987341772L, 0.0},
    {0.3987341772L, 0.0},
    {1.20458891L, 0.0},
    {0.8005082592L, 0.0},
    {0.3987341772L, 0.0},
    {0.6L, 0.0},
    {0.8005082592L, 0.0},
    {0.6L, 0.0},
    {0.3987341772L, 0.0},
    {0.8005082592L, 0.0},
};

static const char* main_options[] = {
    "Select Hotswaps",
    "Toggle Enable",
    "Toggle Disable",
    "Write Command",
    "Exit (q)"
};

static const char* hotswap_touch_list[] = {
    "FLEX_3v3",
    "CDH_3v3",
    "UIUC_3v3",
    "CADET_3v3",
    "RADIO_3v3",
    "VT_3v3",
    "PYRO_3v3",
    "FLEX_7v4",
    "CDH_7v4",
    "UIUC_7v4",
    "CADET_7v4",
    "RADIO_7v4",
    "VTHEAT_7v4",
    "VT_7v4"
};

static const char* hotswap_status_list[] = {
    "CDH_3V3",
    "CDH_7v4",
    "FLEX_3v3",
    "FLEX_7v4",
    "PYRO_3v3",
    "VT_7v4",
    "VT_3v3",
    "VTHEAT_7v4",
    "CADET_3v3",
    "CADET_7v4",
    "RADIO_3v3",
    "RADIO_7v4",
    "UIUC_3v3",
    "UIUC_7v4",
};

static size_t num_main_options = sizeof(main_options) / sizeof(*main_options);
static size_t num_hotswap_options = sizeof(hotswap_touch_list) / sizeof(*hotswap_touch_list);

#define MAIN_WINDOW_NAME ("main_window")
#define MENU_WINDOW_NAME ("menu_window")
#define HOTSWAP_WINDOW_NAME ("hs_window")
#define HOTSWAP_STATUS_WINDOW_NAME ("hss_window")
#define COMMAND_PREVIEW_NAME ("cp_window")
#define COMMAND_STATUS_NAME ("cs_window")
#define BATT_VOLT_WINDOW_NAME ("bv_window")

#define PBTUI_COLOR_WHITE (0)
#define PBTUI_COLOR_RED (COLOR_PAIR(1))
#define PBTUI_COLOR_GREEN (COLOR_PAIR(2))
#define PBTUI_COLOR_MAGENTA (COLOR_PAIR(3))

#define PBTUI_DEBUG (0)

static int pbfd = -1;
static int en = 0;
static int time_to_exit = 0;
static unsigned short hotswap_mask = 0;
static struct window_manager* window_manager = NULL;
static pthread_mutex_t pb_serial_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t pb_window_mutex = PTHREAD_MUTEX_INITIALIZER;
static FILE* err_file = NULL;

void init_ncurses(void);
ITEM** create_main_items(void);
MENU* create_main_menu(ITEM** main_items);
ITEM** create_hotswap_items(void);
MENU* create_hotswap_menu(ITEM** hotswap_items);
void item_selected_print(const char* name);
void choose_hotswap(MENU* hotswap_menu);
void draw_menu(MENU* main_menu, MENU* hotswap_menu);
void set_border(WINDOW* win);
void command_preview_print(MENU* hotswap_menu);
void command_status_print(int successful);
void command_status_clear(void);
void choose_menu(MENU* main_menu, MENU* hotswap_menu);
void pretty_print(WINDOW* win, int y, int x, const char* s, int color);
int hotswap_enable(unsigned short mask);
int hotswap_disable(unsigned short mask);
int hotswap_currents(double* currents);
int hotswap_faults(unsigned short* mask);
void* status_updates(void* arg);
void battery_voltage_update(void);
int battery_voltages(double* voltages);
void battery_voltages_print(double* voltages, int got_voltages);
void hotswap_status_update(void);
void hotswap_currents_print(double* currents, int got_currents);
void hotswap_faults_print(unsigned short fault_mask, int got_faults);
int read_check_bytes(int fd, void* data, size_t* len);

int read_check_bytes(int fd, void* data, size_t* len)
{
    unsigned char* more_data = malloc(*len + 2);
    int ret = EXIT_SUCCESS;
    ssize_t r = read(fd, more_data, *len + 2);
    if(r < 0) {
        fprintf(err_file, "Failed to read anything\n");
        ret = EXIT_FAILURE;
        goto rcb_exit;
    }
    size_t so_far = (size_t)r;
    while(so_far < *len + 2) {
        fprintf(err_file, "Only read %zu/%zu\n", so_far, *len + 2);
        for(size_t i = 0; i < *len + 2; i++) {
            fprintf(err_file, "%zu: %x, ", i, more_data[i]);
        }
        fprintf(err_file, "\n");
        fflush(err_file);
        r = read(fd, more_data + so_far, *len + 2 - so_far);
        if(r < 0) {
            fprintf(err_file, "Failed to read anything\n");
            ret = EXIT_FAILURE;
            goto rcb_exit;
        }
        so_far += (size_t)r;
    }
    if(so_far != *len + 2) {
        fprintf(err_file, "Only read %zu/%zu\n", so_far, *len + 2);
        ret = EXIT_FAILURE;
        goto rcb_exit;
    }
    if(more_data[0] != 0xFB) {
        fprintf(err_file, "Start byte was %x\n", more_data[0]);
        ret = EXIT_FAILURE;
        goto rcb_exit;
    }
    if(more_data[*len + 2 - 1] != 0xFE) {
        fprintf(err_file, "End byte was %x\n", more_data[*len + 2 - 1]);
        ret = EXIT_FAILURE;
        goto rcb_exit;
    }
    memcpy(data, more_data + 1, *len);
rcb_exit:
    free(more_data);
    fflush(err_file);
    return ret;
}

void init_ncurses(void)
{
    initscr();
    start_color();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    init_pair(0, COLOR_WHITE, COLOR_BLACK);
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
}

ITEM** create_main_items(void)
{
    ITEM** main_items = calloc(num_main_options + 1, sizeof(ITEM*));
    for(size_t i = 0; i < num_main_options; i++) {
        main_items[i] = new_item(main_options[i], main_options[i]);
        /* Set the user pointer for debugging */
        void* funcptr;
        CAST_FUNC_TO_VOID_PTR(funcptr, item_selected_print);
        set_item_userptr(main_items[i], funcptr);
    }
    {
        void* choose_hotswap_ptr;
        CAST_FUNC_TO_VOID_PTR(choose_hotswap_ptr, choose_hotswap);
        set_item_userptr(main_items[0], choose_hotswap_ptr);
        main_items[num_main_options] = NULL;
    }
    return main_items;
}

MENU* create_main_menu(ITEM** main_items)
{
    MENU* main_menu = new_menu(main_items);

    WINDOW* menu_window = window_manager_find(window_manager, MENU_WINDOW_NAME);

    set_menu_win(main_menu, menu_window);
    set_menu_sub(main_menu, derwin(menu_window, 14, 22, 1, 1));
    set_menu_mark(main_menu, " * ");
    menu_opts_off(main_menu, O_SHOWDESC);

    return main_menu;
}

ITEM** create_hotswap_items(void)
{
    ITEM** hotswap_items = calloc(num_hotswap_options + 1, sizeof(ITEM*));

    for(size_t j = 0; j < num_hotswap_options; j++) {
        hotswap_items[j] = new_item(hotswap_touch_list[j], hotswap_touch_list[j]);
        void* funcptr;
        CAST_FUNC_TO_VOID_PTR(funcptr, item_selected_print);
        set_item_userptr(hotswap_items[j], funcptr);
    }
    hotswap_items[num_hotswap_options] = NULL;
    item_opts_off(hotswap_items[1], O_SELECTABLE);
    item_opts_off(hotswap_items[8], O_SELECTABLE);

    return hotswap_items;
}

MENU* create_hotswap_menu(ITEM** hotswap_items)
{
    MENU* hotswap_menu = new_menu(hotswap_items);

    WINDOW* hotswap_window = window_manager_find(window_manager, HOTSWAP_WINDOW_NAME);


    set_menu_win(hotswap_menu, hotswap_window);
    set_menu_sub(hotswap_menu, derwin(hotswap_window, 7, 35, 1, 13));
    set_menu_format(hotswap_menu, 7, 2);
    menu_opts_off(hotswap_menu, O_SHOWDESC);
    menu_opts_off(hotswap_menu, O_ROWMAJOR);
    menu_opts_off(hotswap_menu, O_ONEVALUE);
    menu_opts_off(hotswap_menu, O_SHOWMATCH);

    return hotswap_menu;
}

void item_selected_print(const char* name)
{
    WINDOW* main_window = window_manager_find(window_manager, MAIN_WINDOW_NAME);

    move(23, 0);
    clrtoeol();
    mvwprintw(main_window, 23, 0, "Item selected is: %s", name);

    window_manager_refresh(window_manager, MAIN_WINDOW_NAME);
}

void choose_hotswap(MENU* hotswap_menu)
{
    WINDOW* hotswap_window = window_manager_find(window_manager, HOTSWAP_WINDOW_NAME);
    keypad(hotswap_window, TRUE);
    pos_menu_cursor(hotswap_menu);
    int c;
    while((c = wgetch(hotswap_window)) != ENTER) {
        pthread_mutex_lock(&pb_window_mutex);
        switch(c) {
        case KEY_DOWN:
            menu_driver(hotswap_menu, REQ_DOWN_ITEM);
            break;
        case KEY_UP:
            menu_driver(hotswap_menu, REQ_UP_ITEM);
            break;
        case KEY_LEFT:
            menu_driver(hotswap_menu, REQ_LEFT_ITEM);
            break;
        case KEY_RIGHT:
            menu_driver(hotswap_menu, REQ_RIGHT_ITEM);
            break;
        case ' ':
            menu_driver(hotswap_menu, REQ_TOGGLE_ITEM);
            pos_menu_cursor(hotswap_menu);
            command_preview_print(hotswap_menu);
            window_manager_refresh(window_manager, COMMAND_PREVIEW_NAME);
            break;
        default:
            break;
        }
        pthread_mutex_unlock(&pb_window_mutex);
    }
}

void draw_menu(MENU* main_menu, MENU* hotswap_menu)
{
    set_border(window_manager_find(window_manager, MENU_WINDOW_NAME));
    set_border(window_manager_find(window_manager, HOTSWAP_WINDOW_NAME));
    set_border(window_manager_find(window_manager, HOTSWAP_STATUS_WINDOW_NAME));
    set_border(window_manager_find(window_manager, COMMAND_PREVIEW_NAME));
    set_border(window_manager_find(window_manager, COMMAND_STATUS_NAME));
    set_border(window_manager_find(window_manager, BATT_VOLT_WINDOW_NAME));
    
    command_preview_print(hotswap_menu);
    window_manager_refresh_all(window_manager);
    
    post_menu(main_menu);
    post_menu(hotswap_menu);

    window_manager_refresh_all(window_manager);

    time_to_exit = 0;
    pthread_t t;
    pthread_create(&t, NULL, status_updates, &time_to_exit);
    choose_menu(main_menu, hotswap_menu);
    while(time_to_exit) ;

    unpost_menu(hotswap_menu);
    unpost_menu(main_menu);
}

void set_border(WINDOW* win)
{
    wborder(win, '|', '|' , '-', '-', '*', '*', '*', '*');
}

void command_preview_print(MENU* hotswap_menu)
{
    WINDOW* command_window = window_manager_find(window_manager, COMMAND_PREVIEW_NAME);

    pretty_print(command_window, 1, 3, "Current Command:  f000 ", PBTUI_COLOR_WHITE);
    if(en) {
        wattron(command_window, PBTUI_COLOR_GREEN);
        pretty_print(command_window, 1, 26, "02 ", PBTUI_COLOR_WHITE);
        wattroff(command_window, PBTUI_COLOR_GREEN);
    }
    else {
        wattron(command_window, PBTUI_COLOR_RED);
        pretty_print(command_window, 1, 26, "03", PBTUI_COLOR_WHITE);
        wattroff(command_window, PBTUI_COLOR_RED);
    }
    unsigned short c = 0;
    for(int i = 0; i < 14; i++) {
        if(item_value(menu_items(hotswap_menu)[i]) == TRUE) {
            int s = (1 << i);
            c = c | (unsigned short)s;
        }
    }
    hotswap_mask = c;
    wattron(command_window, PBTUI_COLOR_MAGENTA);
    mvwprintw(command_window, 1, 29, "%04hx", c);
    wattroff(command_window, PBTUI_COLOR_MAGENTA);
}

void command_status_print(int successful)
{
    WINDOW* command_status_window = window_manager_find(window_manager, COMMAND_STATUS_NAME);

    if(successful) {
        pretty_print(command_status_window, 1, 2, "Command Successful!", PBTUI_COLOR_GREEN);
    } else {
        pretty_print(command_status_window, 1, 2, "Command Failed!", PBTUI_COLOR_RED);
    }
}

void command_status_clear(void)
{
    WINDOW* command_status_window = window_manager_find(window_manager, COMMAND_STATUS_NAME);

    mvwprintw(command_status_window, 1, 2, "                    ");
}

void choose_menu(MENU* main_menu, MENU* hotswap_menu)
{
    WINDOW* menu_window = window_manager_find(window_manager, MENU_WINDOW_NAME);
    keypad(menu_window, TRUE);
    pos_menu_cursor(main_menu);
    int c;
    while((c = wgetch(menu_window)) != 'q') {
        pthread_mutex_lock(&pb_window_mutex);
        command_status_clear();
        window_manager_refresh(window_manager, COMMAND_STATUS_NAME);
        switch(c) {
        case KEY_DOWN:
            menu_driver(main_menu, REQ_DOWN_ITEM);
            break;
        case KEY_UP:
            menu_driver(main_menu, REQ_UP_ITEM);
            break;
        case ENTER:
        {   
            ITEM* cur = current_item(main_menu);
            void (*p)(const char*);
            CAST_TO_FUNC_PTR(p, item_userptr(cur));
            switch(item_index(cur))
            {
                case 0: /* Enter hs menu */
                    pthread_mutex_unlock(&pb_window_mutex);
                    p((const char*)hotswap_menu);
                    pthread_mutex_lock(&pb_window_mutex);
                    break;
                case 1:
                    en = 1;
                    command_preview_print(hotswap_menu);
                    window_manager_refresh(window_manager, COMMAND_PREVIEW_NAME);
                    break;
                case 2:
                    en = 0;
                    command_preview_print(hotswap_menu);
                    window_manager_refresh(window_manager, COMMAND_PREVIEW_NAME);
                    break;
                case 3:;
                    int ret;
                    if(en) {
                        ret = hotswap_enable(hotswap_mask);
                    } else {
                        ret = hotswap_disable(hotswap_mask);
                    }
                    if(ret) {
                        command_status_print(0);
                    } else {
                        command_status_print(1);
                    }
                    window_manager_refresh(window_manager, COMMAND_STATUS_NAME);
                    break;
                case 4: /* Exit this bitch */
                    goto choose_menu_exit;
                default:
                    pthread_mutex_unlock(&pb_window_mutex);
                    p((const char*)item_name(cur));
                    break;
            }
            pos_menu_cursor(main_menu);
            window_manager_refresh(window_manager, MENU_WINDOW_NAME);
        }
        break;
        default:
            break;
        }
        command_preview_print(hotswap_menu);
        window_manager_refresh_all(window_manager);
        pthread_mutex_unlock(&pb_window_mutex);
    }
choose_menu_exit:
    time_to_exit = 1;
}

void pretty_print(WINDOW* win, int y, int x, const char* s, int color)
{
    if(has_colors() == FALSE)
        return;
    wattron(win, color);
    mvwprintw(win, y, x, s);
    wattroff(win, color);
}

int hotswap_enable(unsigned short mask)
{
    static unsigned char cmd[5] = {
        0xF0, 0x00, 0x02, 0x00, 0x00
    };
    cmd[3] = (unsigned char)(mask >> 8);
    cmd[4] = (unsigned char)(mask & 0xFF);

    pthread_mutex_lock(&pb_serial_mutex);

    int ret;
    if(write(pbfd, cmd, sizeof(cmd)) != sizeof(cmd)) {
        ret = EXIT_FAILURE;
        goto hotswap_enable_exit;
    }
    usleep(100*1000);
    unsigned char resp[5];
    size_t len = sizeof(resp);
    ret = read_check_bytes(pbfd, resp, &len);
    if(ret || len != sizeof(resp)) ret = EXIT_FAILURE;

hotswap_enable_exit:
    pthread_mutex_unlock(&pb_serial_mutex);
    
    return ret;
}

int hotswap_disable(unsigned short mask)
{
    static unsigned char cmd[5] = {
        0xF0, 0x00, 0x03, 0x00, 0x00
    };
    cmd[3] = (unsigned char)(mask >> 8);
    cmd[4] = (unsigned char)(mask & 0xFF);

    pthread_mutex_lock(&pb_serial_mutex);

    int ret;
    if(write(pbfd, cmd, sizeof(cmd)) != sizeof(cmd)) {
        ret = EXIT_FAILURE;
        goto hotswap_disable_exit;
    }
    usleep(100*1000);
    unsigned char resp[5];
    size_t len = sizeof(resp);
    ret = read_check_bytes(pbfd, resp, &len);
    if(ret || len != sizeof(resp)) ret = EXIT_FAILURE;

hotswap_disable_exit:
    pthread_mutex_unlock(&pb_serial_mutex);
    
    return ret;
}

int hotswap_currents(double* currents)
{
    static const unsigned char cmd[3] = {
        0xF0, 0x00, 0x0B
    };

    pthread_mutex_lock(&pb_serial_mutex);
    
    int ret;
    if(write(pbfd, cmd, sizeof(cmd)) != sizeof(cmd)) {
        ret = EXIT_FAILURE;
        fprintf(err_file, "Asking for hotswap currents failed\n");
        goto hotswap_currents_exit;
    }
    usleep(100*1000);
    unsigned char resp[PBD_LEN_HS * 2] = {0};
    size_t len = sizeof(resp);
    ret = read_check_bytes(pbfd, resp, &len);
    if(ret || len != sizeof(resp)) {
        ret = EXIT_FAILURE;
        fprintf(err_file, "Reading hotswap currents failed\n");
        goto hotswap_currents_exit;
    }
    uint16_t* adc = (uint16_t*)resp;
    for(size_t i = 0; i < PBD_LEN_HS; i++) {
        adc[i] = be16toh(adc[i]);
        currents[i] = (double)(pbd_hs_curr_calib[i][0] * adc[i] + pbd_hs_curr_calib[i][1]) / 1000.0;
    }

    ret = EXIT_SUCCESS;

hotswap_currents_exit:
    pthread_mutex_unlock(&pb_serial_mutex);
    fflush(err_file);

    return ret;
}

int hotswap_faults(unsigned short* mask)
{
    static const unsigned char cmd[3] = {
        0xF0, 0x00, 0x0C
    };

    pthread_mutex_lock(&pb_serial_mutex);
    
    int ret;
    if(write(pbfd, cmd, sizeof(cmd)) != sizeof(cmd)) {
        ret = EXIT_FAILURE;
        fprintf(err_file, "Asking for hotswap faults failed\n");
        goto hotswap_faults_exit;
    }
    usleep(100*1000);
    size_t len = sizeof(*mask);
    ret = read_check_bytes(pbfd, mask, &len);
    if(ret || len != sizeof(*mask)) {
        ret = EXIT_FAILURE;
        fprintf(err_file, "Reading hotswap faults failed\n");
        goto hotswap_faults_exit;
    }
    *mask = be16toh(*mask);

    ret = EXIT_SUCCESS;

hotswap_faults_exit:
    pthread_mutex_unlock(&pb_serial_mutex);
    fflush(err_file);

    return ret;
}

void* status_updates(void* arg)
{
    volatile int* should_exit = (volatile int*)arg;
    while(!*should_exit) {
        hotswap_status_update();
        battery_voltage_update();
        sleep(1);
    }
    *should_exit = 0;
    return NULL;
}

void battery_voltage_update(void)
{
    static double voltages[PBD_LEN_PACK];
    memset(voltages, 0, sizeof(voltages));

    int got_voltages = !battery_voltages(voltages);

    pthread_mutex_lock(&pb_window_mutex);

    battery_voltages_print(voltages, got_voltages);

    window_manager_refresh(window_manager, BATT_VOLT_WINDOW_NAME);

    pthread_mutex_unlock(&pb_window_mutex);

}

int battery_voltages(double* voltages)
{
    static const unsigned char cmd[3] = {
        0xF0, 0x00, 0x05
    };

    pthread_mutex_lock(&pb_serial_mutex);
    
    int ret;
    if(write(pbfd, cmd, sizeof(cmd)) != sizeof(cmd)) {
        ret = EXIT_FAILURE;
        goto battery_voltages_exit;
    }
    usleep(100*1000);
    unsigned char resp[PBD_LEN_PACK * 2] = {0};
    size_t len = sizeof(resp);
    ret = read_check_bytes(pbfd, resp, &len);
    if(ret || len != sizeof(resp)) {
        ret = EXIT_FAILURE;
        goto battery_voltages_exit;
    }
    unsigned short* adc = (unsigned short*)resp;
    for(size_t i = 0; i < PBD_LEN_PACK; i++) {
        adc[i] = be16toh(adc[i]);
        voltages[i] = (double)(pbd_pack_volt_calib[i][0]*adc[i] + pbd_pack_volt_calib[i][1]);
    }

    ret = EXIT_SUCCESS;

battery_voltages_exit:
    pthread_mutex_unlock(&pb_serial_mutex);

    return ret;
}

void battery_voltages_print(double* voltages, int got_voltages)
{
    WINDOW* battery_voltage_status_window = window_manager_find(window_manager, BATT_VOLT_WINDOW_NAME);

    static char voltage_strings[PBD_LEN_PACK][10];
    for(size_t i = 0; i < PBD_LEN_PACK; i++) {
        double voltage = voltages[i];
        if(!got_voltages || fabs(voltage) >= 10.0) {
            strcpy(voltage_strings[i], "?????? V");
        } else {
            sprintf(voltage_strings[i], "%+06.3f V", voltage);
        }
    }

    wattron(battery_voltage_status_window, PBTUI_COLOR_WHITE);
    mvwprintw(battery_voltage_status_window, 1, 3, "Battery: %s, %s, %s, %s", voltage_strings[0], voltage_strings[1], voltage_strings[2], voltage_strings[3]);
    wattroff(battery_voltage_status_window, PBTUI_COLOR_WHITE);
}

void hotswap_status_update(void)
{
    static double currents[PBD_LEN_HS];
    static unsigned short fault_mask;
    memset(currents, 0, sizeof(currents));

    int got_currents = !hotswap_currents(currents);
    int got_faults = !hotswap_faults(&fault_mask);

    pthread_mutex_lock(&pb_window_mutex);

    hotswap_currents_print(currents, got_currents);
    hotswap_faults_print(fault_mask, got_faults);
    
    window_manager_refresh(window_manager, HOTSWAP_STATUS_WINDOW_NAME);

    pthread_mutex_unlock(&pb_window_mutex);
}

void hotswap_currents_print(double* currents, int got_currents)
{
    WINDOW* hotswap_status_window = window_manager_find(window_manager, HOTSWAP_STATUS_WINDOW_NAME);

    static char current_strings[PBD_LEN_HS][9];
    for(size_t i = 0; i < PBD_LEN_HS; i++) {
        double current = currents[i];
        if(!got_currents || fabs(current) >= 10.0) {
            strcpy(current_strings[i], "?????? A");
        } else {
            sprintf(current_strings[i], "%+06.3f A", current);
        }
    }

    int x = 4;
    int y = 2;
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 7; j++) {
            pretty_print(hotswap_status_window, y, x, current_strings[(i * 7) + j], PBTUI_COLOR_MAGENTA);
            y++;
        }
        x += 28;
        y = 2;
    }
}

void hotswap_faults_print(unsigned short fault_mask, int got_faults)
{
    WINDOW* hotswap_status_window = window_manager_find(window_manager, HOTSWAP_STATUS_WINDOW_NAME);

    if(!got_faults) fault_mask = 0xFFFF;
    keypad(hotswap_status_window, TRUE);
    int x = 14;
    int y = 2;
    pretty_print(hotswap_status_window, 1, 3, "----------------- Hotswap Status -----------------", PBTUI_COLOR_WHITE);
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 7; j++) {
            int fault_bit;
            if((j + (i * 7)) == 0) {
                fault_bit = fault_mask;
            } else {
                fault_bit = fault_mask >> (j + (i * 7));
            }
            fault_bit &= 0x1;
            int color = fault_bit ? PBTUI_COLOR_RED : PBTUI_COLOR_GREEN;
            pretty_print(hotswap_status_window, y, x, hotswap_status_list[(i * 7) + j], color);
            y++;
        }
        x += 28;
        y = 2;
    }
}

int main(int argc, char** argv)
{
    (void)argc;
    start_serial(&pbfd, argv[1], B9600, 1, 10);
    if(pbfd < 0) {
        printf("Could not open %s", argv[1]);
        return 1;
    }

#if(PBTUI_DEBUG == 1)
    err_file = fopen("pb_err.txt", "w+");
#else
    err_file = fopen("/dev/null", "w");
#endif
    
    init_ncurses();

    window_manager = window_manager_new(newwin(0, 0, 0, 0), MAIN_WINDOW_NAME);

    window_manager_add(window_manager, newwin(18, 24, 0, 0), MENU_WINDOW_NAME, MAIN_WINDOW_NAME);
    window_manager_add(window_manager, newwin(9, 56, 0, 23), HOTSWAP_WINDOW_NAME, MAIN_WINDOW_NAME);
    window_manager_add(window_manager, newwin(10, 56, 8, 23), HOTSWAP_STATUS_WINDOW_NAME, MAIN_WINDOW_NAME);
    window_manager_add(window_manager, newwin(3, 56, 17, 23), COMMAND_PREVIEW_NAME, MAIN_WINDOW_NAME);
    window_manager_add(window_manager, newwin(3, 24, 17, 0), COMMAND_STATUS_NAME, MAIN_WINDOW_NAME);
    window_manager_add(window_manager, newwin(3, 56, 19, 23), BATT_VOLT_WINDOW_NAME, MAIN_WINDOW_NAME);

    ITEM** main_items = create_main_items();
    ITEM** hotswap_items = create_hotswap_items();
    MENU* main_menu = create_main_menu(main_items);
    MENU* hotswap_menu = create_hotswap_menu(hotswap_items);

    draw_menu(main_menu, hotswap_menu);

    for(size_t i = 0; i < num_main_options; ++i) {
        free_item(main_items[i]);
    }
    for(size_t i = 0; i < num_hotswap_options; ++i) {
        free_item(hotswap_items[i]);
    }
    free_menu(hotswap_menu);
    free_menu(main_menu);
    
    free(main_items);
    free(hotswap_items);

    window_manager_free(window_manager, MAIN_WINDOW_NAME);

    free(window_manager);

    endwin();

    fclose(err_file);

    return 0;
}

