#ifndef WINDOW_MANAGER_H_
#define WINDOW_MANAGER_H_

#include <curses.h>
#include <menu.h>

struct window_manager;

struct window_manager* window_manager_new(WINDOW* win, const char* name);
void window_manager_add(struct window_manager* manager, WINDOW* win, const char* name, const char* parent_name);
WINDOW* window_manager_find(struct window_manager* manager, const char* cmp_name);
void window_manager_refresh(struct window_manager* wm, const char* name);
void window_manager_refresh_all(struct window_manager* wm);
void window_manager_free(struct window_manager* manager, const char* name);

#endif

