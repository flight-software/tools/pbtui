#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include <menu.h>

#include "window_manager.h"

struct named_window {
    WINDOW* window;
    char* name;
    struct named_window* parent;
    struct named_window** children;
    size_t num_children;
};

struct window_manager {
    struct named_window* parent;
};

static struct named_window* named_window_new(WINDOW* win, const char* name, struct named_window* parent);
static void named_window_add_child(struct named_window* parent, struct named_window* child);
static struct named_window* named_window_find(struct named_window* parent, const char* cmp_name);
static void named_window_remove(struct named_window* parent, void* ptr);
static void named_window_refresh(struct named_window* parent);
static void named_window_free(struct named_window* nw);

struct named_window* named_window_new(WINDOW* win, const char* name, struct named_window* parent)
{
    struct named_window* nw = malloc(sizeof(struct named_window));
    nw->window = win;
    nw->name = strdup(name);
    nw->parent = parent;
    nw->children = NULL;
    nw->num_children = 0;
    return nw;
}

void named_window_add_child(struct named_window* parent, struct named_window* child)
{
    child->parent = parent;
    if(parent->num_children > 0) {
        for(size_t i = 0; i < parent->num_children; i++) {
            if(!parent->children[i]) {
                parent->children[i] = child;
                return;
            }
        }
    }
    parent->num_children += 1;
    parent->children = realloc(parent->children, parent->num_children * sizeof(struct named_window*));
    parent->children[parent->num_children - 1] = child;
}

struct named_window* named_window_find(struct named_window* parent, const char* cmp_name)
{
    if(!strcmp(parent->name, cmp_name)) {
        return parent;
    }
    for(size_t i = 0; i < parent->num_children; i++) {
        if(parent->children[i]) {
            struct named_window* found = named_window_find(parent->children[i], cmp_name);
            if(found) return found;
        }
    }
    return NULL;
}

void named_window_remove(struct named_window* parent, void* ptr)
{
    for(size_t i = 0; i < parent->num_children; i++) {
        if(parent->children[i] == ptr) {
            parent->children[i] = NULL;
            return;
        }
    }
}

void named_window_refresh(struct named_window* parent)
{
    wrefresh(parent->window);
    for(size_t i = 0; i < parent->num_children; i++) {
        if(parent->children[i]) {
            named_window_refresh(parent->children[i]);
        }
    }
}

void named_window_free(struct named_window* parent)
{
    for(size_t i = 0; i < parent->num_children; i++) {
        if(parent->children[i]) {
            named_window_free(parent->children[i]);
        }
        free(parent->children[i]);
    }
    free(parent->children);
    free(parent->name);
    delwin(parent->window);
}

struct window_manager* window_manager_new(WINDOW* win, const char* name)
{
    struct window_manager* wm = malloc(sizeof(struct window_manager));
    wm->parent = named_window_new(win, name, NULL);
    return wm;
}

void window_manager_add(struct window_manager* manager, WINDOW* win, const char* name, const char* parent_name)
{
    struct named_window* parent = named_window_find(manager->parent, parent_name);
    struct named_window* new = named_window_new(win, name, parent);
    named_window_add_child(parent, new);
}

WINDOW* window_manager_find(struct window_manager* manager, const char* cmp_name)
{
    struct named_window* nw = named_window_find(manager->parent, cmp_name);
    if(nw) return nw->window;
    return NULL;
}

void window_manager_refresh(struct window_manager* wm, const char* name)
{
    struct named_window* parent = named_window_find(wm->parent, name);
    named_window_refresh(parent);
}

void window_manager_refresh_all(struct window_manager* wm)
{
    named_window_refresh(wm->parent);
}

void window_manager_free(struct window_manager* manager, const char* name)
{
    struct named_window* to_delete = named_window_find(manager->parent, name);
    if(!to_delete) return;
    named_window_free(to_delete);
    if(to_delete->parent) {
        struct named_window* to_delete_parent = to_delete->parent;
        named_window_remove(to_delete_parent, to_delete);
    }
    free(to_delete);
}

